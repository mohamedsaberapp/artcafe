import { Component } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { AuthPage } from "../pages/auth/auth";
import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = AuthPage;


  constructor(platform: Platform, public storage: Storage, public statusBar: StatusBar, splashScreen: SplashScreen, private oneSignal: OneSignal) {
    platform.ready().then(() => {
      // OneSignal Code start:
      if (platform.is('cordova')) {
        statusBar.styleDefault();
        splashScreen.hide();
        var notificationOpenedCallback = function (jsonData) {
          console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };
        this.oneSignal.startInit('3f0ff650-5c65-43db-97e6-ad9516304994', '193081998876');
        this.oneSignal.handleNotificationOpened().subscribe(() => {
          // do something when a notification is opened
          console.log('opened');
        });
        this.oneSignal.endInit();
      }
    }
    )
  }
}
