import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the StudiosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StudiosProvider {

  constructor(public http: Http) {
    console.log('Hello StudiosProvider Provider');
  }

  getStudios(){
    return this.http.get('http://artcafe.bit68.com/clubs').map(res => res.json());
  }
}
