import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ClassSessionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ClassSessionsProvider {

  constructor(public http: Http) {
    console.log('Hello ClassSessionsProvider Provider');
  }

  getSessions(class_id) {
    return this.http.get('http://artcafe.bit68.com/class_sessions/?class_id=' + class_id).map(res => res.json());
  }
}
