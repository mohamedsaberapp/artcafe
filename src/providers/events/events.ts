import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the EventsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EventsProvider {

  constructor(public http: Http) {
    console.log('Hello EventsProvider Provider');
  }

  getEvents(){
    return this.http.get('http://artcafe.bit68.com/events').map(res => res.json());
  }
}
