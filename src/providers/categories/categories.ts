import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CategoriesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategoriesProvider {

  constructor(public http: Http) {
    console.log('Hello CategoriesProvider Provider');
  }

  getCategories(){
    return this.http.get('http://artcafe.bit68.com/all_classes').map(res => res.json());
  }


}
