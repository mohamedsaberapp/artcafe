import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the UserinfoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserinfoProvider {

  constructor(public http: Http) {
    console.log('Hello UserinfoProvider Provider');
  }

  getUserInfo(token){
    return  this.http.get('http://artcafe.bit68.com/user_info/?token=' + token).map(res => res.json());
  }
}
