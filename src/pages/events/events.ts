import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { SessiondetailsPage } from "../sessiondetails/sessiondetails";
import { BookPage } from "../book/book";
import { SubcatPage } from "../subcat/subcat";
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { EventsProvider } from '../../providers/events/events';
/**
 * Generated class for the EventsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {
  dropins: any[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http:Http,
    public spinnerDialog:SpinnerDialog,
    private toastCtrl:ToastController,
    private events:EventsProvider
  ) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AllprogramsPage');
    this.spinnerDialog.show('Loading...');
    this.events.getEvents().subscribe(data => {
      this.spinnerDialog.hide();
      console.log(data)
      this.dropins=data;
    }, err => {
      this.spinnerDialog.hide();
      let toast=this.toastCtrl.create({
        message:err,
        duration:2000
      });
      toast.present();
      console.log(err);
    });
  }
  addEvent() {
    this.navCtrl.push(EventsPage);
  }
  sessDet(s) {
    this.navCtrl.push(SessiondetailsPage, { data: s });
  }
  catDet(c) {
    this.navCtrl.push(SubcatPage, { data: c });
  }
  goBook(s) {
    this.navCtrl.push(BookPage, { data: s });
  }
}
