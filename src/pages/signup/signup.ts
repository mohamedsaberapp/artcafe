import { Component } from '@angular/core';
import { NavController, NavParams, Events, ToastController, Platform } from 'ionic-angular';
import { Http } from "@angular/http";
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { TabsPage } from "../tabs/tabs";
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PasswordValidator } from '../../validators/password.validator';
import { Country } from './form.model';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { FCM } from '@ionic-native/fcm';
import { OneSignal } from '@ionic-native/onesignal';

/**
 * Generated class for the SignupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  myPlayerId: string;
  country_phone_group: FormGroup;
  countries: Array<Country>;
  genders: Array<string>;
  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  noMatch: boolean;
  RegForm: FormGroup;
  myInfo: any;

  constructor(
    public navCtrl: NavController,
    public http: Http,
    public storage: Storage,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public events: Events,
    public spinnerDialog: SpinnerDialog,
    private one: OneSignal,
    private toastCtrl: ToastController,
    private platform: Platform
  ) {
    //Get player ID for one signal service
    if (this.platform.is('cordova')) {
      this.one.getIds().then(val => {
        console.log(val);
        this.myPlayerId = val.userId;
      })
    }
    //Register form
    this.validations_form = this.formBuilder.group({
      name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.compose([
        Validators.minLength(11),
        Validators.maxLength(11),
        Validators.required])),
      city: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: new FormGroup({
        password: new FormControl('', Validators.compose([
          Validators.minLength(6),
          Validators.required,
        ])),
        confirm_password: new FormControl('', Validators.required)
      }, (formGroup: FormGroup) => {
        return PasswordValidator.areEqual(formGroup);
      }),
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  //Validation messages
  validation_messages = {
    'city': [
      { type: 'required', message: 'City is required' }
    ],
    'name': [
      { type: 'required', message: 'First name is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'phone': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'minlength', message: 'Phone must be 11 characters long.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Password mismatch' }
    ],
  };

  goHome() {
    this.navCtrl.push(TabsPage);
  }
  onSubmit(values) {
    this.spinnerDialog.show('Loading ...');
    //Signup request
    this.http.get('http://artcafe.bit68.com/register/?email=' + values.email + '&password=' + values.matching_passwords.password + '&name=' + values.name + '&phone_number=' + values.phone + '&city=' + values.city + '&player_id=' + this.myPlayerId).map(res => res.json()).subscribe(data => {
      this.spinnerDialog.hide();
      //check for token
      if (data.token) {
        console.log(data);
        //set user data in storage
        this.storage.set('user_info',data);
        this.events.publish('user:logged', data);
        this.navCtrl.setRoot(TabsPage);
      } else {
        let toast = this.toastCtrl.create({
          message: "Please make sure you filled all fields correctly !",
          duration: 2000
        });
        toast.present();
      }
    }, err => {
      this.spinnerDialog.hide();
      console.log(err);
      let toast = this.toastCtrl.create({
        message: err._body,
        duration: 2000
      });
      toast.present();
    });
  }
}
