import { Component } from '@angular/core';
import { NavController, NavParams, Events, ToastController } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { Storage } from '@ionic/storage';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { OneSignal } from '@ionic-native/onesignal';
import { Platform } from 'ionic-angular/platform/platform';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myPlayerId: string;
  deviceToken: string;
  regInfo: any;
  loginForm: FormGroup;
  userData: any;
  constructor(
    public navCtrl: NavController,
    public http: Http,
    public storage: Storage,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public events: Events,
    public spinnerDialog: SpinnerDialog,
    private one: OneSignal,
    private toastCtrl: ToastController,
    private platform: Platform
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });

    //Get Player ID for oneSignal Service
    if (this.platform.is('cordova')) {
      this.one.getIds().then(val => {
        console.log(val);
        this.myPlayerId = val.userId;
      })
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  doLog() {
    this.spinnerDialog.show('Loading ...');
    //Login request
    this.http.get('http://artcafe.bit68.com/login/?email=' + this.loginForm.value.email + '&password=' + this.loginForm.value.password + '&player_id=' + this.myPlayerId).subscribe((data: any) => {
      this.spinnerDialog.hide();
      console.log('login info', data);
      if (data._body == 'Invalid login credentials') {
        let toast = this.toastCtrl.create({
          message: data._body,
          duration: 2000
        });
        toast.present();
      } else {
        this.userData = data.json();
        console.log(this.userData);
        //Store user info in local storage to remember user
        this.storage.set('user_info', JSON.stringify(this.userData));
        this.events.publish('user:logged', this.userData);
        this.navCtrl.setRoot(TabsPage);
      }
    }, err => {
      this.spinnerDialog.hide();
      console.log(err);
      let toast = this.toastCtrl.create({
        message: err._body,
        duration: 2000
      });
      toast.present();
    });
  }

}
