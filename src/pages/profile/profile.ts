import { Component } from '@angular/core';
import { NavController, NavParams, Events, ToastController } from 'ionic-angular';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { UserinfoProvider } from '../../providers/userinfo/userinfo';
/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  picId: any;
  fileTransfer: FileTransferObject = this.transfer.create();

  myPic: any;
  noEvents: boolean;
  noNotifs: boolean;
  noStudios: boolean;
  noBooks: boolean;
  myInfo: any = {
    club_bookings: [],
    class_bookings: [],
    event_history: []
  };
  user_info: any = {};
  choice: string = "History";


  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public navParams: NavParams,
    public events: Events,
    public http: Http,
    public spinnerDialog: SpinnerDialog,
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private toastCtrl: ToastController,
    private userInfo: UserinfoProvider
  ) {

  }
  updateInfo() {
    this.spinnerDialog.show('Loading...');
    this.storage.get('user_info').then((val) => {
      this.user_info = JSON.parse(val);
      console.log(this.user_info);
      this.userInfo.getUserInfo(this.user_info.token).subscribe(data => {
        this.spinnerDialog.hide();
        this.myInfo = data;
        console.log('updated info', data);
      }, err => {
        this.spinnerDialog.hide();
        console.log(err);
        let toast = this.toastCtrl.create({
          message: err,
          duration: 2000
        });
        toast.present();
      })
    })
  }

  ionViewDidEnter() {
    this.updateInfo();
  }

  doRefresh(f) {
    this.updateInfo();
    f.complete();
  }

  updatePic() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 300,
      targetHeight: 300,
    }

    this.camera.getPicture(options).then(file_uri => {
      console.log(file_uri);
      this.myPic = file_uri;
      let transfer_options: FileUploadOptions = {
        fileKey: 'file',
        fileName: 'name.jpeg',
        chunkedMode: false,
        mimeType: 'image/jpeg'
      }
      this.spinnerDialog.show();
      this.fileTransfer.upload(file_uri, 'http://artcafe.bit68.com/file_transfer/', transfer_options)
        .then((data) => {
          this.spinnerDialog.hide();
          console.log('transfer success', data);
          this.picId = data.response;

          this.http.get('http://artcafe.bit68.com/update_profile/?user_id=' + this.user_info.token + '&picture_id=' + this.picId).map(res => res.json()).subscribe(data => {
            this.spinnerDialog.hide();
            console.log(data);
            console.log('updated info', data);
            this.updateInfo();
          }, err => {
            this.spinnerDialog.hide();
            let toast = this.toastCtrl.create({
              message: err,
              duration: 2000
            });
            toast.present();
            console.log(err);
          })
        }, (err) => {
          this.spinnerDialog.hide();
          let toastTransfer = this.toastCtrl.create({
            message: err,
            duration: 2000
          });
          toastTransfer.present();
          console.log('transfer err', err)
        })

    }, err => {
      let toastGetPic = this.toastCtrl.create({
        message: err,
        duration: 2000
      });
      toastGetPic.present();
      console.log('get picture', err);
    });
  }

}
