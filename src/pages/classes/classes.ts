import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { ProgramdetailsPage } from "../programdetails/programdetails";
import { ClassdetailsPage } from "../classdetails/classdetails";
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { ClassSessionsProvider } from '../../providers/class-sessions/class-sessions';

/**
 * Generated class for the ClassesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-classes',
  templateUrl: 'classes.html',
})
export class ClassesPage {
  itemsCairo: any;
  itemsGiza: any;
  dotw: any;
  class_id: any;
  categories: { name: string; img: string; desc: string; }[];
  myZayed: any;
  myMaadi: any;
  zayed: any;
  maadi: any;
  choice: string;
  classes: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http:Http,
    public spinnerDialog:SpinnerDialog,
    private toastCtrl:ToastController,
    private classSessions:ClassSessionsProvider
  ) {
    this.choice = 'Cairo';
    this.myMaadi=[];
    this.itemsCairo=[];
    this.itemsGiza=[];
    this.myZayed=[];
    this.class_id=this.navParams.get('id');
    this.dotw=[
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ] 
  }
  initializeItemsCairo() {
    this.itemsCairo = this.myMaadi;
  }

  onInputCairo(ev: any) {
    // Reset items back to all of the items
    this.initializeItemsCairo();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.itemsCairo = this.itemsCairo.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  initializeItemsGiza() {
    this.itemsGiza = this.myZayed;
  }

  onInputGiza(ev: any) {
    // Reset items back to all of the items
    this.initializeItemsGiza();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.itemsGiza = this.itemsGiza.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  pickDate(e) {
    this.myMaadi=e.Maadi;
    this.myZayed=e.Zayed;
    for (var x = 0; x < this.classes.length; x++) {
      this.classes[x].color = 'primary'
    }
    e.color = "light";
    console.log(e.day, e.date);
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AllprogramsPage');
    this.spinnerDialog.show('Loading...');
    this.classSessions.getSessions(this.class_id).subscribe(data => {
      this.spinnerDialog.hide();
      console.log(data);
      this.classes=data;
      
      this.myMaadi=data.Maadi;
      this.myZayed=data.Zayed;
      this.initializeItemsCairo();
      this.initializeItemsGiza();
      
    }, err => {
      console.log(err);
      this.spinnerDialog.hide();
      let toast=this.toastCtrl.create({
        message:err,
        duration:2000
      });
      toast.present();
     
    });
  }
  progDet(e) {
    this.navCtrl.push(ProgramdetailsPage, { data: e });
  }
  goDet(c) {
    this.navCtrl.push(ClassdetailsPage, { data: c });
  }
  goBook(s) {
    this.navCtrl.push(ClassdetailsPage, { data: s });
  }
}
