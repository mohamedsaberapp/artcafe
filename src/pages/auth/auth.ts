import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { SignupPage } from "../signup/signup";
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the AuthPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {
  constructor(public navCtrl: NavController, public storage: Storage, public navParams: NavParams) {
    //Check if the user is already logged in!
    this.storage.get('user_info').then((val) => {
      if (val) {
        this.navCtrl.setRoot(TabsPage);
      }
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AuthPage');
  }
  doLog() {
    this.navCtrl.push(LoginPage);
  }
  doReg() {
    this.navCtrl.push(SignupPage);
  }
}
